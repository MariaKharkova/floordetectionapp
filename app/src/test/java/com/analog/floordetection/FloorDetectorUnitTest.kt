package com.analog.floordetection

import android.hardware.SensorManager
import com.analog.floordetection.data.FloorDetector
import com.analog.floordetection.data.preferences.AppPreferences
import org.junit.Test
import org.mockito.Mockito

class FloorDetectorUnitTest {
    private val appPreferences: AppPreferences = Mockito.mock(AppPreferences::class.java)
    private val sensorManager = Mockito.mock(SensorManager::class.java)
    private val floorDetector = FloorDetector(appPreferences)

    @Test
    fun floorLevelAfterInitIsZeroTest() {
        Mockito.intThat { floorDetector.getFloorNumber().value == 0 }
    }
}