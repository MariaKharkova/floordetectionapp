package com.analog.floordetection

import android.app.Application
import com.analog.floordetection.di.FloorDetectionComponent

class FloorDetectionApp : Application() {
    companion object {
        @JvmStatic lateinit var floorDetectionComponent : FloorDetectionComponent
    }

    override fun onCreate() {
        super.onCreate()
        floorDetectionComponent = FloorDetectionComponent(this)
    }
}