package com.analog.floordetection.annotations

/**
 * Uses to declare part of logic that is required but isn't full implemented in the current version
 */
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION,
    AnnotationTarget.VALUE_PARAMETER, AnnotationTarget.EXPRESSION, AnnotationTarget.PROPERTY
)
@Retention(AnnotationRetention.SOURCE)
@MustBeDocumented
annotation class LogicNotImplemented