package com.analog.floordetection.di

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import android.preference.PreferenceManager
import com.analog.floordetection.data.BarometerManager
import com.analog.floordetection.data.FloorDetector
import com.analog.floordetection.data.preferences.AppPreferences
import com.analog.floordetection.repository.FloorDetectionRepository

class FloorDetectionComponent(private val appContext: Context) {
    private companion object {
        private val sync = Any()
    }

    private var _appPreferences : AppPreferences? =null
    val appPreferences: AppPreferences
        get() {
            synchronized(sync) {
                _appPreferences = _appPreferences ?: AppPreferences(PreferenceManager.getDefaultSharedPreferences(appContext))
                return _appPreferences as AppPreferences
            }
        }

    private var _floorDetector : FloorDetector? =null
    val floorDetector: FloorDetector
        get() {
            synchronized(sync) {
                _floorDetector = _floorDetector ?: FloorDetector(appPreferences)
                return _floorDetector as FloorDetector
            }
        }

    private var _sensorManager: SensorManager? = null
    private val sensorManager: SensorManager
        get() {
            synchronized(sync) {
                _sensorManager = _sensorManager ?: appContext.getSystemService(Context.SENSOR_SERVICE) as SensorManager
                return _sensorManager as SensorManager
            }
        }

    private var _sensor: Sensor? = null
    private val sensor: Sensor?
        get() {
            synchronized(sync) {
                _sensor = _sensor ?: sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE)
                return _sensor as Sensor
            }
        }

    private var _barometerManager: BarometerManager? = null
    val barometerManager: BarometerManager
        get() {
            synchronized(sync) {
                _barometerManager = _barometerManager ?: BarometerManager(sensorManager, sensor)
                return _barometerManager as BarometerManager
            }
        }

    private var _floorDetectionRepository: FloorDetectionRepository? = null
    val floorDetectionRepository: FloorDetectionRepository
        get() {
            synchronized(sync) {
                _floorDetectionRepository = _floorDetectionRepository ?: FloorDetectionRepository(appPreferences, floorDetector)
                return _floorDetectionRepository as FloorDetectionRepository
            }
        }
}