package com.analog.floordetection.data

import android.hardware.SensorManager
import androidx.lifecycle.MutableLiveData
import com.analog.floordetection.annotations.LogicNotImplemented
import com.analog.floordetection.data.preferences.AppPreferences

class FloorDetector constructor(private val appPreferences: AppPreferences) {

    private var floorNumberData: MutableLiveData<Int> = MutableLiveData()
    /**
     * indicates that the first floor  pressure was initialized
     */
    private var initialized = false
    private var floorHeight = appPreferences.getFloorHeight()
    @LogicNotImplemented
    private var groundFloorHeight = appPreferences.getGroundFloorHeight()
    /**
     * do not react on height changes if they are smaller than debounceChangeValue
     */
    @LogicNotImplemented
    private var debounceChangeValue = 0.0f
    /**
     * distance between current floor and the first floor
     */
    private var altitudeDeltaValue = 0.0f
    private var newFloorNumber = 0
    private var currentFloorNumber = 0


    private var firstFloorPressure = 0.0f

    /**
     * @return if floor was changed returns Pair, where the first value is floorNumber and the second indicates is direction up
     */
    fun detectFloorNumberChanges(pressure: Float): Pair<Int, Boolean>? {
        if (initialized) {
            altitudeDeltaValue = SensorManager.getAltitude(SensorManager.PRESSURE_STANDARD_ATMOSPHERE, pressure) -
                    SensorManager.getAltitude(SensorManager.PRESSURE_STANDARD_ATMOSPHERE, firstFloorPressure)
            newFloorNumber = countFloorNumber(altitudeDeltaValue)
            if (newFloorNumber != currentFloorNumber) {
                floorNumberData.value = newFloorNumber
                val result = Pair(newFloorNumber, newFloorNumber > currentFloorNumber)
                currentFloorNumber = newFloorNumber
                return result
            }
        } else {
            initializeDetectionSettings(pressure)
        }
        return null
    }

    private fun countFloorNumber(heightDifference: Float): Int {
        return Math.round(heightDifference / floorHeight)
    }

    private fun initializeDetectionSettings(pressure: Float) {
        initialized = true
        floorHeight = appPreferences.getFloorHeight()
        firstFloorPressure = pressure
        floorNumberData.value = currentFloorNumber
    }

    fun setupInitialState() {
        altitudeDeltaValue = 0.0f
        firstFloorPressure = 0.0f
        currentFloorNumber = 0
        newFloorNumber = 0
        initialized = false
        floorNumberData.value = 0
    }

    fun getFloorNumber() = floorNumberData

}