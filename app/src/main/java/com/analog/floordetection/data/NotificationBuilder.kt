package com.analog.floordetection.data

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ContentResolver
import android.content.Intent
import android.media.AudioAttributes
import android.net.Uri
import android.os.Build
import androidx.core.app.NotificationCompat
import com.analog.floordetection.R


private const val notificationRequestCode = 0
private const val channelName = "FloorDetectionChannel"
internal const val notificationIdIdentifier = 1
var notification: Notification? = null

fun BarometerForegroundService.showInitialNotification(): Notification? {
    notification = buildInitialNotification(getString(R.string.floor_detection_running)).build()
    return notification
}


fun BarometerForegroundService.showFloorNotification(
    floor: Int,
    isUpMove: Boolean
): Notification? {
    val soundUri =
        Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + packageName + "/" + if (isUpMove) R.raw.upsound else R.raw.downsound)
    notification = buildInitialNotification(String.format(getString(R.string.your_floor_message), floor), soundUri).build()
    return notification
}

private fun BarometerForegroundService.buildInitialNotification(
    message: String,
    soundUri: Uri? = null
): NotificationCompat.Builder {
    val receiverAction = applicationContext.packageName + ".SERVICE_FOREGROUND_NOTIFICATION"
    createNotificationChannel(soundUri)
    val intentAction = Intent()
    intentAction.action = receiverAction
    intentAction.`package` = packageName
    val pendingIntent =
        PendingIntent.getBroadcast(this,
            notificationRequestCode, intentAction, PendingIntent.FLAG_UPDATE_CURRENT)
    val notificationBuilder = NotificationCompat.Builder(this, channelName)
        .setSmallIcon(android.R.drawable.ic_menu_compass)
        .setContentText(message)
        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
        .setContentIntent(pendingIntent)
        .setOngoing(true)
        .setOnlyAlertOnce(true)
    soundUri?.let {
        notificationBuilder.setSound(soundUri)
    }
    return notificationBuilder
}

private fun BarometerForegroundService.createNotificationChannel(soundUri: Uri? = null) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val name = channelName
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(channelName, name, importance)
        soundUri.let {
            val attributes = AudioAttributes.Builder()
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                .build()
            channel.setSound(it, attributes)
        }
        channel.enableVibration(true)
        val notificationManager = getSystemService(NotificationManager::class.java)
        notificationManager!!.createNotificationChannel(channel)
    }
}
