package com.analog.floordetection.data

import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.os.IBinder
import com.analog.floordetection.FloorDetectionApp
const val stopServiceIntentParam = "stop"
class BarometerForegroundService : Service(), SensorEventListener {

    private var notificationManager: NotificationManager? = null

    private val barometerManager = FloorDetectionApp.floorDetectionComponent.barometerManager

    private val floorDetector = FloorDetectionApp.floorDetectionComponent.floorDetector

    private val appPreferences = FloorDetectionApp.floorDetectionComponent.appPreferences

    private var sensorsActive = false

    override fun onCreate() {
        super.onCreate()
        initSensorState()
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    private fun initSensorState() {
        sensorsActive = appPreferences.getIsServiceActive()
    }

    private fun handleStopSelfFromIntent(): Int {
        notificationManager?.cancel(notificationIdIdentifier)
        stopForeground(true)
        appPreferences.setServiceState(false)
        this.stopSelf()
        barometerManager.stop()
        return START_NOT_STICKY
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        sensorsActive = appPreferences.getIsServiceActive()
        if ((intent?.extras?.get(stopServiceIntentParam) != null) || (sensorsActive)) {
            return handleStopSelfFromIntent()
        }
        intent?.let {
            processServiceStartForDataCollection()
            return START_STICKY
        }
        stopForeground(true)
        return START_NOT_STICKY
    }

    private fun processServiceStartForDataCollection() {
        buildInitialNotification()
        startForegroundAndSensorData()
    }

    private fun startForegroundAndSensorData() {
        try {
            startForeground(notificationIdIdentifier, notification)
            appPreferences.setServiceState(true)
            startProcessingBarometerSensorData()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun buildInitialNotification() {
        notificationManager?.notify(notificationIdIdentifier, showInitialNotification())
    }

    private fun buildFloorNumberNotification(currentFloor: Int, isUpMove: Boolean) {
        notificationManager?.notify(notificationIdIdentifier, showFloorNotification(currentFloor, isUpMove))
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        // Do nothing
    }

    override fun onSensorChanged(event: SensorEvent?) {
        // Do nothing
    }

    private fun startProcessingBarometerSensorData() {
        barometerManager.start(object : BarometerManager.BarometerPressureListener {
            override fun onPressureChanged(pressure: Float) {
               val result = floorDetector.detectFloorNumberChanges(pressure)
                result?.let {  buildFloorNumberNotification(it.first, it.second)}
            }
        })
    }

    override fun onBind(intent: Intent?): IBinder {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}