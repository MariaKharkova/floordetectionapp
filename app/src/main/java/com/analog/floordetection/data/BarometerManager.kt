package com.analog.floordetection.data

import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager


class BarometerManager constructor(private val sensorManager: SensorManager, private val sensor: Sensor?) :
    SensorEventListener {

    interface BarometerPressureListener {
        fun onPressureChanged(pressure: Float)
    }

    private var listener: BarometerPressureListener? = null

    fun start(listener: BarometerPressureListener) {
        this.listener = listener
        sensorManager.registerListener(
            this,
            sensor,
            SensorManager.SENSOR_DELAY_UI
        )
    }

    fun stop() {
        sensorManager.unregisterListener(this)
        listener = null
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        // Do nothing...
    }

    override fun onSensorChanged(event: SensorEvent?) {
        event?.values?.get(0)?.let { listener?.onPressureChanged(it) }
    }
}