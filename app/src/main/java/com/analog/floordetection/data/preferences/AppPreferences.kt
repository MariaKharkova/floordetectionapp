package com.analog.floordetection.data.preferences

import android.content.SharedPreferences
import com.analog.floordetection.annotations.LogicNotImplemented

private const val PREFERENCES_SERVICE_IS_ACTIVE = "com.analog.floordetection.data.preferences.IS_SERVICE_ACTIVE"
private const val PREFERENCES_FLOOR_HEIGHT = "com.analog.floordetection.data.preferences.PREFERENCES_FLOOR_HEIGHT"
private const val PREFERENCES_GROUND_FLOOR_HEIGHT = "com.analog.floordetection.data.preferences.PREFERENCES_GROUND_FLOOR_HEIGHT"
private const val PREFERENCES_CURRENT_FLOOR_NUMBER = "com.analog.floordetection.data.preferences.PREFERENCES_CURRENT_FLOOR_NUMBER"
private const val DEFAULT_FLOOR_HEIGHT = 3.0f
private const val DEFAULT_GROUND_FLOOR_HEIGHT = DEFAULT_FLOOR_HEIGHT
private const val DEFAULT_CURRENT_FLOOR_NUMBER  = 0

class AppPreferences constructor(private val preferences: SharedPreferences) {

    fun setServiceState(isActive: Boolean) = preferences.edit().putBoolean(PREFERENCES_SERVICE_IS_ACTIVE, isActive).apply()

    fun getIsServiceActive() = preferences.getBoolean(PREFERENCES_SERVICE_IS_ACTIVE, false)

    fun setFloorHeight(height: Float) = preferences.edit().putFloat(PREFERENCES_FLOOR_HEIGHT, height).apply()

    fun getFloorHeight() = preferences.getFloat(PREFERENCES_FLOOR_HEIGHT, DEFAULT_FLOOR_HEIGHT)

    @LogicNotImplemented
    fun setGroundFloorHeight(height: Float) = preferences.edit().putFloat(PREFERENCES_GROUND_FLOOR_HEIGHT, height).apply()

    @LogicNotImplemented
    fun getGroundFloorHeight() = preferences.getFloat(PREFERENCES_GROUND_FLOOR_HEIGHT, DEFAULT_GROUND_FLOOR_HEIGHT)

    @LogicNotImplemented
    fun getCurrentFloorNumber() = preferences.getInt(PREFERENCES_CURRENT_FLOOR_NUMBER, DEFAULT_CURRENT_FLOOR_NUMBER)
}