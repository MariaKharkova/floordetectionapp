package com.analog.floordetection.repository

import com.analog.floordetection.data.FloorDetector
import com.analog.floordetection.annotations.LogicNotImplemented
import com.analog.floordetection.data.preferences.AppPreferences


class FloorDetectionRepository (private val appPreferences: AppPreferences, private val floorDetector: FloorDetector){

    fun getFloorNumber() = floorDetector.getFloorNumber()

    fun getFloorHeight() =  appPreferences.getFloorHeight()

    @LogicNotImplemented
    fun getGroundFloorHeight() = appPreferences.getGroundFloorHeight()

    fun isFloorDetectionRunning() = appPreferences.getIsServiceActive()

    fun updateBuildingParams(floorHeight: Float, groundFloorHeight: Float) {
        appPreferences.setFloorHeight(floorHeight)
        appPreferences.setGroundFloorHeight(groundFloorHeight)
    }

    fun setupInitialStateForFloorDetection() = floorDetector.setupInitialState()
}