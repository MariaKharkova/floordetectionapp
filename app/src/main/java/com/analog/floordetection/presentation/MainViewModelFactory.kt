package com.analog.floordetection.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.analog.floordetection.FloorDetectionApp

@Suppress("UNCHECKED_CAST")
class MainViewModelFactory: ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MainViewModel::class.java)) {
            return MainViewModel(FloorDetectionApp.floorDetectionComponent.floorDetectionRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}