package com.analog.floordetection.presentation.helper

import android.content.Context
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.analog.floordetection.R

/**
 * Show dialog if a device doesn't have barometer
 */
fun Context.showNoBarometerDetectedAlert(){
    val dialogBuilder = AlertDialog.Builder(this)
    dialogBuilder.setMessage(getString(R.string.no_barometer))
        .setCancelable(false)
        .setPositiveButton(android.R.string.ok) { dialog, _ ->  dialog.cancel()
        }
    val alert = dialogBuilder.create()
    alert.setTitle(getString(R.string.no_barometer_dialog_title))
    alert.show()
 }

/**
 * Show aler dialog if floor height is too small
 */
fun Context.showInappropriateFloorHeightAlert(){
    Toast.makeText(this, getString(R.string.alert_floor_height_min), Toast.LENGTH_SHORT).show()
}