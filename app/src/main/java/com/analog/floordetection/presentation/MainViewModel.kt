package com.analog.floordetection.presentation

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.analog.floordetection.annotations.LogicNotImplemented
import com.analog.floordetection.data.BarometerForegroundService
import com.analog.floordetection.data.stopServiceIntentParam
import com.analog.floordetection.repository.FloorDetectionRepository
private const val MINIMUM_FLOOR_HEIGHT = 2
class MainViewModel constructor(private val floorDetectionRepository: FloorDetectionRepository) :
    ViewModel() {
    private val barometerAvailability: MutableLiveData<Boolean> = MutableLiveData()
    private val floorValue = floorDetectionRepository.getFloorNumber()
    private var floorHeight = floorDetectionRepository.getFloorHeight()

    fun isBarometerAvailable() = barometerAvailability

    fun getCurrentFloor() = floorValue

    fun isFloorDetectionRunning() = floorDetectionRepository.isFloorDetectionRunning()

    fun getFloorHeight() = floorHeight

    fun isFloorHeightValid(height: Float) = height>=MINIMUM_FLOOR_HEIGHT

    @LogicNotImplemented
    fun getGroundFloorHeight() = floorDetectionRepository.getGroundFloorHeight()

    /**
     * currently updates only  floorHeight
     */
    @LogicNotImplemented
    private fun updateBuildingParams(floorHeight: Float, groundFloorHeight: Float)  = floorDetectionRepository.updateBuildingParams(floorHeight, groundFloorHeight)

    fun startTrackingService(context: Context,floorHeight: Float) {
        updateBuildingParams(floorHeight, 0.0f)
        if (checkIsBarometerSensorAvailable(context)) {
            val newIntent = Intent(context, BarometerForegroundService::class.java)
            context.startService(newIntent)
        }
    }

    fun stopTrackingService(context: Context) {
        floorDetectionRepository.setupInitialStateForFloorDetection()
        val newIntent = Intent(context, BarometerForegroundService::class.java)
        newIntent.putExtra(stopServiceIntentParam, true)
        context.startService(newIntent)
    }


    private fun checkIsBarometerSensorAvailable(context: Context): Boolean {
        val isExist = context.packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_BAROMETER)
        barometerAvailability.value = isExist
        return isExist
    }


}