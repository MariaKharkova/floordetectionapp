package com.analog.floordetection.presentation

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.analog.floordetection.R
import com.analog.floordetection.presentation.helper.showInappropriateFloorHeightAlert
import com.analog.floordetection.presentation.helper.showNoBarometerDetectedAlert
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var viewModel: MainViewModel

    private val barometerAvailabilityObserver = Observer<Boolean> { response ->
        response?.let { isAvailable ->
            if (!isAvailable) {
                disableUsageIfBarometerNotFound()
            }
            viewModel.isBarometerAvailable().removeObservers(this)
        }
    }

    private val currentFloorObserver = Observer<Int> { response ->
        response?.let {
            floorNumber.text = it.toString()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProviders.of(this, MainViewModelFactory()).get(MainViewModel::class.java)
        viewModel.isBarometerAvailable().observe(this, barometerAvailabilityObserver)
        viewModel.getCurrentFloor().observe(this, currentFloorObserver)
        initViews()
    }

    private fun initViews() {
        if (viewModel.isFloorDetectionRunning()) {
            enableTracking.isChecked = true
            rippleBackground.startRippleAnimation()
            floorHeightField.isEnabled = false
        }
        setUpSwitchTracker()
        floorHeightField.setText(viewModel.getFloorHeight().toString())
        floorHeightField.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(value: Editable?) {
                val stringValue = value.toString()
                updateUIOnFloorHeightChanged(if(stringValue.isNotEmpty()) { viewModel.isFloorHeightValid(stringValue.toFloat()) } else false)
            }
        })
    }

    private fun updateUIOnFloorHeightChanged(validValue: Boolean){
        enableTracking.isEnabled = validValue
        if (!validValue){
            showInappropriateFloorHeightAlert()
        }
    }


    private fun setUpSwitchTracker() {
        enableTracking.setOnCheckedChangeListener { _, enable ->
            floorHeightField.isEnabled = !enable
            if (!enable) {
                viewModel.stopTrackingService(this)
                viewModel.getCurrentFloor().removeObservers(this)
                rippleBackground.stopRippleAnimation()
            } else {
                viewModel.startTrackingService(this, floorHeightField.text.toString().toFloat())
                viewModel.getCurrentFloor().observe(this, currentFloorObserver)
                rippleBackground.startRippleAnimation()
            }

        }
    }

    private fun disableUsageIfBarometerNotFound() {
        showNoBarometerDetectedAlert()
        enableTracking.isEnabled = false
        enableTracking.setOnCheckedChangeListener(null)
        enableTracking.isChecked = false
    }
}
